﻿using examenT3.DB.Mapping;
using examenT3.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace examenT3.DB
{
    public class EjercicioContext : DbContext
    {
        public DbSet<Ejercicio> Bloggers { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<NuevaRutina> NuevaRutinas { get; set; }
        public DbSet<Detalle> Detalles { get; set; }

        public EjercicioContext(DbContextOptions<EjercicioContext> options) : base(options) { }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new EjercicioMap());
            modelBuilder.ApplyConfiguration(new AccountMap());
            modelBuilder.ApplyConfiguration(new DetalleMap());
            modelBuilder.ApplyConfiguration(new NuevaRutinaMap());

        }
    }

}
