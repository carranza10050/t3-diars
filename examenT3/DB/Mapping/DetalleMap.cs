﻿using examenT3.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace examenT3.DB.Mapping
{
    public class DetalleMap : IEntityTypeConfiguration<Detalle>
    {
        public void Configure(EntityTypeBuilder<Detalle> builder)
        {
            builder.ToTable("Detalle");
            builder.HasKey(o => o.Id);

            builder.HasOne(o => o.ejerc)
                .WithMany()
                .HasForeignKey(o => o.IdEjercicio);
        }
    }
}
