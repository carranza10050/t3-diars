﻿using examenT3.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace examenT3.DB.Mapping
{
    public class NuevaRutinaMap : IEntityTypeConfiguration<NuevaRutina>
    {
        public void Configure(EntityTypeBuilder<NuevaRutina> builder)
        {
            builder.ToTable("NuevaRutina");
            builder.HasKey(o => o.Id);

            //builder.HasOne(o => o.)
            //    .WithMany()
            //    .HasForeignKey(o => o.);
        }
    }
}
