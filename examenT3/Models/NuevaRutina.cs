﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace examenT3.Models
{
    public class NuevaRutina
    {
        public int Id { get; set; }
        public int IdAccount { get; set; }
        public string Nombre { get; set; }
        public string Tipo { get; set; }
    }
}
