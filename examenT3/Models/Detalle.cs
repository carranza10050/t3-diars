﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace examenT3.Models
{
    public class Detalle
    {
        public int Id { get; set; }
        public int IdNuevaRutina { get; set; }
        public int IdEjercicio { get; set; }
        public int Tiempo { get; set; }
        public Ejercicio ejerc { get; set; }
    }
}
