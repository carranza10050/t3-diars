﻿using examenT3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace examenT3.Estrategia
{
    public class Principiante : IRutinaEjer

    {
        public List<Detalle> Rutina(int cantidad, List<Ejercicio> ejercicio, int id)
        {
            Random random = new Random();
            int maxvalue = ejercicio.Count();
            List<Detalle> Detalles = new List<Detalle>();
            for (int i = 0; i < cantidad; i++)
            {
                var detalle = new Detalle();
                var ejercicios = random.Next(1, maxvalue);
                detalle.IdEjercicio = ejercicios;
                detalle.IdNuevaRutina = id;
                detalle.Tiempo = random.Next(60, 120);

                Detalles.Add(detalle);

            }
            return Detalles;
        }
    }
    public class Intermedio : IRutinaEjer
    {
        public List<Detalle> Rutina(int cantidad, List<Ejercicio> ejercicio, int id)
        {
            Random random = new Random();
            int maxvalue = ejercicio.Count();
            List<Detalle> Detalles = new List<Detalle>();
            for (int i = 0; i < cantidad; i++)
            {
                var detalle = new Detalle();
                var ejercicios = random.Next(1, maxvalue);
                detalle.IdEjercicio = ejercicios;
                detalle.IdNuevaRutina = id;
                detalle.Tiempo = random.Next(60, 120);

                Detalles.Add(detalle);
            }
            return Detalles;
        }
    }
    public class Avanzado : IRutinaEjer
    {
        public List<Detalle> Rutina(int cantidad, List<Ejercicio> ejercicio, int id)
        {
            Random random = new Random();
            int maxvalue = ejercicio.Count();
            List<Detalle> Detalles = new List<Detalle>();
            for (int i = 0; i < cantidad; i++)
            {
                var detalle = new Detalle();
                var ejercicios = random.Next(1, maxvalue);
                detalle.IdEjercicio = ejercicios;
                detalle.IdNuevaRutina = id;
                detalle.Tiempo = random.Next(120, 120);

                Detalles.Add(detalle);
            }
            return Detalles;
        }
    }
}
