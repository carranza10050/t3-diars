﻿using examenT3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace examenT3.Estrategia
{
    interface IRutinaEjer
    {
        public List<Detalle> Rutina(int cantidad, List<Ejercicio> ejercicio, int id);
    }
}
