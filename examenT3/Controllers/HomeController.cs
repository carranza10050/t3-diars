﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using examenT3.Models;
using examenT3.DB;
using examenT3.Estrategia;
using Microsoft.EntityFrameworkCore;

namespace examenT3.Controllers
{

    public class HomeController : Controller
    {
        private IRutinaEjer ir;

        private readonly EjercicioContext context;
        public HomeController(EjercicioContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {

            return View();
        }
        [HttpGet]
        public IActionResult Crear()
        {

            return View();
        }
        [HttpPost]
        public IActionResult Crear(NuevaRutina nr)
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = context.Accounts.First(o => o.Usuario == username);

            nr.IdAccount = user.Id;
            context.NuevaRutinas.Add(nr);
            context.SaveChanges();

            var ejercicio = context.Bloggers.ToList();
            int cantidadEjercicios = 0;
            switch (nr.Tipo)
            {
                case "Principiante":
                    ir = new Principiante();
                    cantidadEjercicios = 5;
                    break;
                case "Intermedio":
                    ir = new Intermedio();
                    cantidadEjercicios = 10;
                    break;
                case "Avanzado":
                    ir = new Avanzado();
                    cantidadEjercicios = 15;
                    break;
                default:
                    break;
            }

            var guardar = ir.Rutina(cantidadEjercicios,ejercicio, nr.Id);
            context.Detalles.AddRange(guardar);
            context.SaveChanges();

            return RedirectToAction("Index");
        }
        public IActionResult detalle()
        {
            var bloggers = context.NuevaRutinas;
            
            return View(bloggers);
        }

        public IActionResult detalleRutina(int idRutina)
        {
            ViewBag.Nombre = context.NuevaRutinas.Where(o => o.Id == idRutina).FirstOrDefault();
            var bloggers = context.Detalles.Include(o => o.ejerc).Where(o => o.IdNuevaRutina == idRutina).ToList();

            return View(bloggers);
        }
    }
}
